<?php
$servername = "127.0.0.1";
$username = "root";
$password = "";
$dbname = "contoh";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Koneksi gagal: " . $conn->connect_error);
}

$searchUsername = isset($_GET['search_username']) ? $_GET['search_username'] : '';

$whereClause = '';
if (!empty($searchUsername)) {
    $searchUsername = mysqli_real_escape_string($conn, $searchUsername);
    $whereClause = "WHERE username LIKE '%$searchUsername%'";
}

$sql = "SELECT * FROM users $whereClause";
$result = $conn->query($sql);

if (!$result) {
    die("Query salah: " . $conn->error);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/admin.css">
    <title>Table Users</title>
</head>
<body>
<div class="search-container">
        <form method="get">
            <input type="text" name="search_username" placeholder="Search by Username" value="<?php echo $searchUsername; ?>">
            <button type="submit">Search</button>
        </form>
    </div>

    <div class="regis">
        <button class="register" type="sumbit"><a href="registerByAdmin.php">Register</a></button>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Gender</th>
                <th>Email</th>
                <th>Username</th>
                <th>Password</th>
                <th colspan="2" >Tindakan</th>
            </tr>
        </thead>
        <tbody>
        <?php
        while ($row = $result->fetch_assoc()) {
            echo "<tr>";
            echo "<td>" . $row["id"] . "</td>";
            $genderText = ($row["gender"] == 1) ? "Man" : (($row["gender"] == 2) ? "Girl" : "Others");
            echo "<td>" . $genderText . "</td>";            
            echo "<td>" . $row["email"] . "</td>";
            echo "<td>" . $row["username"] . "</td>";
            echo "<td>" . $row["password"] . "</td>";
            echo "<td><a href='editByAdmin.php?id=" . $row["id"] . "'>Edit</a></td>";
            echo "<td><a href='deleteByAdmin.php?id=" . $row["id"] . "'>Delete</a></td>";
            echo "</tr>";
        }
        ?>
    </tbody>
    </table>

</body>
</html>