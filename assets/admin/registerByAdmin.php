<?php
$servername = "127.0.0.1";
$username = "root";
$password = "";
$dbname = "contoh";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Koneksi gagal: " . $conn->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $gender = $_POST['gender'];
    $email = $_POST['email'];
    $username = $_POST['username'];
    $password = md5($_POST['password']);

    $sql = "INSERT INTO users (id, first_name, last_name, gender, email, username, password) VALUES ('$id', '$first_name', '$last_name', '$gender', '$email', '$username', '$password')";
    $result = $conn->query($sql);

    if ($result) {
        header("Location: adminPage.php");
    } else {
        die("Query salah: " . $conn->error);
    }

    $conn->close();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/registerByAdmin.css">
    <title>Register By Admin</title>
</head>
<body>
    <div class="container">
    <div class="text">
        <p>Register Form</p>
    </div>
        <form action="registerByAdmin.php" method="post">
            <div class="form-row">
                <div class="input-data">
                    <input type="text" name="id" required>
                    <div class="underline"></div>
                    <label for="id">Id</label>
                </div>

                <div class="input-data">
                    <input type="text" name="first_name" required>
                    <div class="underline"></div>
                    <label for="first_name">First Name</label>
                </div>
            </div>
            <div class="form-row">
                <div class="input-data">
                    <input type="text" name="last_name" required>
                    <div class="underline"></div>
                    <label for="last_name">Last Name</label>
                </div>

                <div class="input-data">
                    <input type="text" name="gender" required>
                    <div class="underline"></div>
                    <label for="gender">Gender</label>
                </div>
            </div>

            <div class="form-row">
                <div class="input-data">
                    <input type="text" name="email" required>
                    <div class="underline"></div>
                    <label for="email">Email</label>
                </div>

                <div class="input-data">
                    <input type="text" name="username" required>
                    <div class="underline"></div>
                    <label for="username">username</label>
                </div>
            </div>

            <div class="form-row">
                <div class="input-data">
                    <input type="text" name="password" required>
                    <div class="underline"></div>
                    <label for="password">password</label>
                </div>
            </div>

            <div class="form-row submit-btn">
                <div class="input-data">
                    <div class="inner"></div>
                    <input type="submit" value="submit">
                </div>
            </div>
        </form>
    </div>
</body>
</html>