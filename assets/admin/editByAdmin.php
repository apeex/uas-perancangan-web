<?php
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "contoh";

    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Koneksi gagal: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM users WHERE id = '$id'";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();

    if (!$row) {
        die("Tidak ada data");
    }

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $id = $_POST['id'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $gender = $_POST['gender'];
        $email = $_POST['email'];
        $username = $_POST['username'];
        $password = md5($_POST['password']);

        if (empty($first_name)) {
            die("First Name cannot be empty");
        }
        
        $sqlupdate = "UPDATE users SET id = ?, first_name = ?, last_name = ?, gender = ?, email = ?, username = ?, password = ? WHERE id = ?";
        $stmt = $conn->prepare($sqlupdate);
        $stmt->bind_param("sssssssi", $id, $first_name, $last_name, $gender, $email, $username, $password, $id);

        if ($stmt->execute()) {
            header("Location: adminPage.php");
        } else {
            die("Query salah: " . $conn->error);
        }
    }
    $conn->close();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/editByAdmin.css">
    <title>Edit By Admin</title>
</head>
<body>
<div class="container">
    <div class="text">
        <p>Update Form</p>
    </div>
        <form action="" method="post">
            <div class="form-row">
                <div class="input-data">
                    <input type="text" name="id" value="<?php echo $row['id']; ?>">
                    <div class="underline"></div>
                    <label for="id">Id</label>
                </div>

                <div class="input-data">
                    <input type="text" name="first_name" value="<?php echo $row['first_name']; ?>">
                    <div class="underline"></div>
                    <label for="first_name">First Name</label>
                </div>
            </div>
            <div class="form-row">
                <div class="input-data">
                    <input type="text" name="last_name" value="<?php echo $row['last_name']; ?>">
                    <div class="underline"></div>
                    <label for="last_name">Last Name</label>
                </div>

                <div class="input-data">
                <input type="text" name="gender" value="<?php echo $row['gender']; ?>">
                    <div class="underline"></div>
                    <label for="gender">Gender</label>
                </div>
            </div>

            <div class="form-row">
                <div class="input-data">
                    <input type="text" name="email" value="<?php echo $row['email']; ?>">
                    <div class="underline"></div>
                    <label for="email">Email</label>
                </div>

                <div class="input-data">
                    <input type="text" name="username" value="<?php echo $row['username']; ?>">
                    <div class="underline"></div>
                    <label for="username">Username</label>
                </div>
            </div>

            <div class="form-row">
                <div class="input-data">
                    <input type="text" name="password" value="<?php echo $row['password']; ?>">
                    <div class="underline"></div>
                    <label for="password">Password</label>
                </div>
            </div>

            <div class="form-row submit-btn">
                <div class="input-data">
                    <div class="inner"></div>
                    <input type="submit" value="Update">
                </div>
            </div>
        </form>
    </div>
    
</body>
</html>